package de.cbay

import de.cbay.GameResult.*
import de.cbay.Player.Companion.PlayerStats
import de.cbay.RockPaperScissor.*
import org.testng.annotations.Test

import org.testng.Assert.*
import org.testng.annotations.DataProvider

/**
 * Testing [Player].
 *
 * @author Christian Bay (christian.bay@posteo.net)
 */
class PlayerTest {
    @DataProvider(name = DATA_FOR_MAKE_PLAY)
    private fun createDataForMakePlay(): Array<Array<Any?>> =
        arrayOf(
            arrayOf(
                Player.STRATEGY_ALWAYS_ROCK,
                true,
                ROCK
            ),
            arrayOf(
                Player.STRATEGY_RANDOM,
                false,
                null
            )
        )

    @Test(dataProvider = DATA_FOR_MAKE_PLAY)
    fun testPlayAndUpdateRecord(
        strategy: () -> RockPaperScissor,
        isDeterministic: Boolean,
        expectedResult: RockPaperScissor?
    ) {
        val player = Player(TEST_PLAYER)
        repeat((0..9).count()) {
            val receivedResult = player.makePlay(strategy)
            if (isDeterministic) {
                assertEquals(receivedResult, expectedResult!!)
            }
            player.updateGameResultRecord(WIN)
        }

        val gameRecord = player.getGameRecord()
        assertEquals(10, gameRecord.allGamesPlayed)
        assertEquals(10, gameRecord.gamesWon)
    }

    @DataProvider(name = DATA_FOR_TEST_GAME_RECORD)
    private fun createDataForGameRecord(): Array<Array<Any>> =
        arrayOf(
            arrayOf(
                listOf(WIN, LOSS, DRAW, DRAW, DRAW).toMutableList(),
                listOf(SCISSOR, ROCK, PAPER, PAPER, PAPER).toMutableList(),
                PlayerStats(
                    TEST_PLAYER,
                    gamesWon = 1,
                    gamesLost = 1,
                    gamesDrawn = 3,
                    allGamesPlayed = 5,
                    chosenRock = 1,
                    chosenScissor = 1,
                    chosenPaper = 3,
                    wonWithPaper = 0,
                    wonWithScissor = 1,
                    wonWithRock = 0
                )
            ),
            arrayOf(
                listOf(WIN, WIN, DRAW, DRAW, DRAW).toMutableList(),
                listOf(SCISSOR, SCISSOR, PAPER, PAPER, PAPER).toMutableList(),
                PlayerStats(
                    TEST_PLAYER,
                    gamesWon = 2,
                    gamesLost = 0,
                    gamesDrawn = 3,
                    allGamesPlayed = 5,
                    chosenRock = 0,
                    chosenScissor = 2,
                    chosenPaper = 3,
                    wonWithPaper = 0,
                    wonWithScissor = 2,
                    wonWithRock = 0
                )
            )
        )

    @Test(dataProvider = DATA_FOR_TEST_GAME_RECORD)
    fun testGetGameRecord(
        gameHistory: MutableList<GameResult>,
        gamePlays: MutableList<RockPaperScissor>,
        expectedPlayerStats: PlayerStats
    ) {
        // given
        val player = Player(TEST_PLAYER, gameHistory, gamePlays)

        // when
        val gameRecord = player.getGameRecord()

        // then
        assertEquals(gameRecord, expectedPlayerStats)
    }

    companion object {
        private const val DATA_FOR_TEST_GAME_RECORD = "DATA_FOR_TEST_GAME_RECORD"
        private const val DATA_FOR_MAKE_PLAY = "DATA_FOR_MAKE_PLAY"
        private const val TEST_PLAYER = "TEST"
    }
}