package de.cbay

import kotlin.math.absoluteValue
import kotlin.random.Random

/**
 * Represents a player with its game stats.
 *
 * @author Christian Bay (christian.bay@posteo.net)
 */
class Player internal constructor(
    private val playerName: String,
    private var results: MutableList<GameResult> = mutableListOf(),
    private var plays: MutableList<RockPaperScissor> = mutableListOf()
    ) {

    fun makePlay(strategy: () -> RockPaperScissor): RockPaperScissor {
        return strategy.apply { }().also { plays.add(it) }
    }

    fun updateGameResultRecord(gameResult: GameResult) {
        results.add(gameResult)
        return
    }

    fun getGameRecord(): PlayerStats {
        val allGames = plays.count()
        val decisiveGames = results.filterNot { it == GameResult.DRAW }
        val drawnGames = allGames - decisiveGames.count()
        val wonGames = decisiveGames.filter { it == GameResult.WIN }.count()
        val lostGames = allGames - drawnGames - wonGames

        val successPerPlayType =
            results
                .zip(plays)
                .foldRight(mutableMapOf<RockPaperScissor, Int>()) { (gameResult, play), acc ->
                    if (gameResult == GameResult.WIN) {
                        acc[play] = acc[play]?.plus(1) ?: 1
                        acc
                    } else {
                        acc
                    }
                }

        return PlayerStats(
            playerName = playerName,
            gamesWon = wonGames,
            gamesLost = lostGames,
            gamesDrawn = drawnGames,
            allGamesPlayed = allGames,
            chosenPaper = plays.filter { it == RockPaperScissor.PAPER }.count(),
            chosenRock = plays.filter { it == RockPaperScissor.ROCK }.count(),
            chosenScissor = plays.filter { it == RockPaperScissor.SCISSOR }.count(),
            wonWithPaper = successPerPlayType[RockPaperScissor.PAPER] ?: 0,
            wonWithScissor = successPerPlayType[RockPaperScissor.SCISSOR] ?: 0,
            wonWithRock = successPerPlayType[RockPaperScissor.ROCK] ?: 0
        )
    }

    companion object {
        val STRATEGY_RANDOM = { RockPaperScissor.values()[Random.nextInt().absoluteValue % 3] }
        val STRATEGY_ALWAYS_ROCK = { RockPaperScissor.ROCK }

        data class PlayerStats(
            val playerName: String,
            val gamesWon: Int,
            val gamesLost: Int,
            val gamesDrawn: Int,
            val allGamesPlayed: Int,
            val chosenRock: Int,
            val chosenScissor: Int,
            val chosenPaper: Int,
            val wonWithPaper: Int,
            val wonWithScissor: Int,
            val wonWithRock: Int,
        ) {
            fun printResults() {
                println("Player Stats for $playerName:")
                println("Overall games played: ${this.allGamesPlayed}")
                println("Overall games won: ${this.gamesWon}")
                println("Overall games lost: ${this.gamesLost}")
                println("Overall games drawn: ${this.gamesDrawn}")
                RockPaperScissor.values().forEach {
                    print("Success rate with $it: ")
                    when (it) {
                        RockPaperScissor.ROCK -> println(wonWithRock)
                        RockPaperScissor.PAPER -> println(wonWithPaper)
                        RockPaperScissor.SCISSOR -> println(wonWithScissor)
                    }
                }
            }
        }
    }
}