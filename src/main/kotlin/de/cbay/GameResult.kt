package de.cbay

/**
 * All possible game results.
 *
 * @author Christian Bay (christian.bay@posteo.net)
 */
enum class GameResult {
    WIN,
    DRAW,
    LOSS;

    fun invert(): GameResult =
        when (this) {
            WIN -> LOSS
            DRAW -> DRAW
            LOSS -> WIN
        }

}