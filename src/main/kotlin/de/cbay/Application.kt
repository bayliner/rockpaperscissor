package de.cbay

/**
 * A very simple Rock-Paper-Scissor simulation.
 *
 * @author Christian Bay (christian.bay@posteo.net)
 */
fun main() {
    val player1 = Player("Player 1")
    val player2 = Player("Player 2")

    repeat((0..99).count()) {
        val play1 = player1.makePlay(Player.STRATEGY_ALWAYS_ROCK)
        val play2 = player2.makePlay(Player.STRATEGY_RANDOM)
        val gameResult = RockPaperScissor.evaluateGameRound(play1, play2)
        player1.updateGameResultRecord(gameResult)
        player2.updateGameResultRecord(gameResult.invert())
    }

    val gameRecordPlayer1 = player1.getGameRecord()
    val gameRecordPlayer2 = player2.getGameRecord()

    println("FINAL RESULTS:")
    println("Number of games ${gameRecordPlayer1.playerName} won: ${gameRecordPlayer1.gamesWon}")
    println("Number of games ${gameRecordPlayer2.playerName} won: ${gameRecordPlayer2.gamesWon}")
    println("Draws: ${gameRecordPlayer1.gamesDrawn}")
    println("-------------------------")
    println("-------------------------")
    gameRecordPlayer1.printResults()
    println("-------------------------")
    gameRecordPlayer2.printResults()
}